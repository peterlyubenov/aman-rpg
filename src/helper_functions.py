"""
Written on 15-Dec-2020
by Aman Miah
"""



# Replace all generic pronouns in the text with the player's pronouns
def replace_pronouns(text, character):
    new_text = text
    for pronoun in ["[heshe]", "[himher]", "[hisher]", "[hishers]"]:
        new_text = new_text.replace(pronoun, character.pronouns[pronoun])
    
    return new_text

