"""
Written on 15-Dec-2020
by Aman Miah
"""
class QuestObjective:
    def __init__(self, description, objective_id, optional = False):
        self.description = description
        self.completed = False
        self.optional = optional
        self.id = objective_id

        self.on_start()
        
    
    def complete(self):
        self.completed = True
        self.on_complete()
    
    def on_start(self):
        pass

    def on_complete(self):
        pass

class Quest:
    def __init__(self, name):
        self.name = name
        self.objectives = []
        self.completed = False
        self.init()

    def init(self):
        pass
    
    def add_objective(self, objective):
        self.objectives.append(objective)
    
    def complete_objective(self, objective_id):
        self.check_complete()

        for objective in self.next_objectives():
            if objective.id == objective_id:
                objective.complete()
                self.check_complete()
    
    def check_complete(self):
        if len(self.next_objectives()) == 0:
            self.complete()
            return True

        return False

    def complete(self):
        self.completed = True
        self.on_complete()
    
    def on_complete(self):
        pass
    
    def __str__(self):
        string =  f"   --==[{self.name}]==--   \n\n"
        
        for obj in self.next_objectives():
            string += f"- {obj.description}"
            if obj.optional:
                string += " (Optional)"
            
            string += "\n"
        return string
    
    # Return a list of any optional objectives and a single non-optional objective to complete next
    def next_objectives(self):
        next_objectives = []
        for obj in self.objectives:
            if not obj.completed:
                next_objectives.append(obj)

                if not obj.optional:
                    return next_objectives
        return []


if __name__ == "__main__":
    class GetUpObjective(QuestObjective):
        def on_complete(self):
            print("You successfully got up!")
    
    class GetTurkey(Quest):
        def init(self):       
            self.add_objective(GetUpObjective("Get up from the chair", "get_up"))
            self.add_objective(QuestObjective("Go pee", "pee", optional = True))
            self.add_objective(QuestObjective("Get turkey", "get_turkey"))
        
        def on_complete(self):
            print("You successfully got turkey! Congratulations!")


    quest = GetTurkey("Get turkey (idk)")
    print(quest)

    while True:
        user_input = input("> ").split(" ")
        command = user_input[0]
        args = user_input[1:]

        if command == "complete":
            quest.complete_objective(args[0])

        if not quest.completed:
            print(quest)
