from global_variables import globalvars
from Character import PlayerCharacter
from quests import Quest, QuestObjective

# Create character
globalvars.main_character = PlayerCharacter("Peter", gender="m")
character = globalvars.main_character


# Create quests
class DragonQuest(Quest):
    def init(self):
        self.add_objective(QuestObjective("Reach the dragon's lair", "reach_lair"))
        self.add_objective(QuestObjective("Slay the dragon", "slay_dragon"))


class PrincessQuest(Quest):
    def init(self):
        self.add_objective(QuestObjective("Get to the highest tower", "reach_tower"))
        self.add_objective(QuestObjective("Lead the princess to safety", "rescue_princess"))
        self.add_objective(QuestObjective("Kiss the princess", "kiss_princess"))


character.start_quest(DragonQuest("Defeat the evil dragon"))
character.start_quest(PrincessQuest("Rescue the princess"))

# Main loop
while True:
    user_input = input("> ")

    if "journal" in user_input:
        character.print_quests()
