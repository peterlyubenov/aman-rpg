"""
Written on 15-Dec-2020
by Aman Miah
"""
import helper_functions
from global_variables import globalvars


class Character:
    def __init__(self, name, gender = "m"):
        self.name = name
        self.gender = gender
        self.inventory = []
        self.health = 100
        self.max_health = 100

        self.initialize_pronouns()
    
    def initialize_pronouns(self):
        self.pronouns = {
            "[heshe]": "he" if self.gender == "m" else "she",
            "[himher]": "him" if self.gender == "m" else "her",
            "[hisher]": "his" if self.gender == "m" else "her",
            "[hishers]": "his" if self.gender == "m" else "hers"
        }
    
    # Show what a character is saying in a fancy way
    def speak(self, text):
        chara = globalvars.main_character
        # How many spaces should new lines be indented to make it consistent
        spaces = " " * (len(self.name) + 2)

        lines = text.split("\n")
        print(f"{self.name}: {helper_functions.replace_pronouns(lines[0], chara)}")

        for line in lines[1:]:
            print(f"{spaces}{helper_functions.replace_pronouns(line, chara)}")
    
    def heal(self, amount):
        self.health += amount
        if self.health > self.max_health:
            self.health = self.max_health
    
    def harm(self, amount):
        self.health -= amount
        if self.health < 0:
            self.health = 0;

class PlayerCharacter(Character):

    def __init__(self, name, gender="m"):
        super().__init__(name, gender=gender)

        self.quests = []

    def start_quest(self, quest):
        self.quests.append(quest)

    def quest_list(self):
        return [x.name for x in self.quests]
    
    def print_quests(self):
        print("Active quests:")
        for q in self.quest_list():
            print("- " + q)