"""
Written on 15-Dec-2020
by Aman Miah
"""
from global_variables import globalvars

class Item:
    pass

class Potion(Item):
    def consume(self):
        print(f"You drink the {self.name}")

        self.effect()
    
class HealingPotion(Potion):
    def __init__(self):
        self.name = "Healing potion"
    
    def effect(self):
        chara = globalvars.main_character

        heal_amount = int(chara.max_health * 0.2)
        chara.heal(heal_amount)
        print(f"You restore {heal_amount} points of health. Health: {chara.health}")
